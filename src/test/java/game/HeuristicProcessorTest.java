package game;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class HeuristicProcessorTest {

    @Test
    public void calculateTest() {
        final HeuristicProcessor heuristicProcessor = new HeuristicProcessor();

        final Board board = new Board();
        board.cross(0, 0, Sign.Y);
        board.cross(0, 1, Sign.X);
        board.cross(0, 2, Sign.Y);
        board.cross(1, 0, Sign.X);
        board.cross(1, 1, Sign.X);
        board.cross(2, 0, Sign.Y);

        final Board board2 = new Board();
        board2.cross(0, 0, Sign.Y);
        board2.cross(0, 1, Sign.X);
        board2.cross(0, 2, Sign.Y);
        board2.cross(2, 1, Sign.X);
        board2.cross(1, 1, Sign.X);
        board2.cross(2, 0, Sign.Y);

        final int calculate = heuristicProcessor.calculate(board);
        final int calculate2 = heuristicProcessor.calculate(board2);

        board.printBoard();
        System.out.println(calculate);
        board2.printBoard();
        System.out.println(calculate2);

        Assertions.assertThat(calculate2).isGreaterThan(calculate);
    }

    @Test
    public void calculateTest2() {
        final HeuristicProcessor heuristicProcessor = new HeuristicProcessor();

        final Board board = new Board();
        board.cross(0, 0, Sign.Y);
        board.cross(0, 1, Sign.X);
        board.cross(0, 2, Sign.X);
        board.cross(1, 1, Sign.X);
        board.cross(2, 0, Sign.Y);


        final Board board2 = new Board();
        board2.cross(0, 0, Sign.Y);
        board2.cross(0, 1, Sign.X);
        board2.cross(0, 2, Sign.X);
        board2.cross(1, 0, Sign.X);
        board2.cross(2, 0, Sign.Y);

        final int calculate = heuristicProcessor.calculate(board);
        final int calculate2 = heuristicProcessor.calculate(board2);

        board.printBoard();
        System.out.println(calculate);
        board2.printBoard();
        System.out.println(calculate2);

        Assertions.assertThat(calculate2).isGreaterThan(calculate);
    }

    @Test
    public void calculateTest3() {
        final HeuristicProcessor heuristicProcessor = new HeuristicProcessor();

        final Board board = new Board();
        board.cross(0, 0, Sign.Y);
        board.cross(0, 2, Sign.X);

        final Board board2 = new Board();
        board2.cross(0, 0, Sign.Y);
        board2.cross(1, 1, Sign.X);


        final int calculate = heuristicProcessor.calculate(board);
        final int calculate2 = heuristicProcessor.calculate(board2);

        board.printBoard();
        System.out.println(calculate);
        board2.printBoard();
        System.out.println(calculate2);

        Assertions.assertThat(calculate2).isGreaterThan(calculate);
    }
}
