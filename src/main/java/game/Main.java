package game;

import com.scalified.tree.TreeNode;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {

    final static MoveGenerator moveGenerator = new MoveGenerator();

    public static void main(String[] args) {

        final Board board = new Board();

//        board.cross(0, 0, Sign.Y);
        //board.setMoveFor(Sign.X);
//
//        board.printBoard();

        playersMove(board);

        Board board1 = computerMove(board);
        playersMove(board1);

        Board board2 = computerMove(board1);
        playersMove(board2);

        Board board3 = computerMove(board2);
        playersMove(board3);

        Board board4 = computerMove(board3);
        playersMove(board4);
    }

    private static Board playersMove(Board board) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Your move: ");

        String s = keyboard.nextLine();

        board.cross(Integer.valueOf(s.split(" ")[0]), Integer.valueOf(s.split(" ")[1]), Sign.Y);
        board.printBoard();

        return board;
    }

    private static Board computerMove(Board board) {
        TraversalProvider traversalProvider = new TraversalProvider();
        final TreeNode<BoardTreeNode> boardTreeNodeTreeNode = moveGenerator.generateMovesTree(board);

        boardTreeNodeTreeNode.traversePreOrder(traversalProvider.traversalAction());

        System.out.println("Computer move: ");
        System.out.print("Deep: " + boardTreeNodeTreeNode.height());
        System.out.println(" Nodes: " + countNodes(boardTreeNodeTreeNode));
        Board board1 = boardTreeNodeTreeNode.data().getBoard();
        board1.printBoard();

        return board1;
    }



    public static int countNodes(TreeNode<BoardTreeNode> boardTreeNodeTreeNode) {
        return IntStream.iterate(1, x -> x + 1)
                .limit(boardTreeNodeTreeNode.height())
                .reduce(1, (x, y) -> x * y);
    }
}
