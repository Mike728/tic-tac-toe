package game;

public enum Sign {
    X(" X "), Y(" Y "), EMPTY_CELL("   ");

    private String value;

    Sign(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
