package game;

public class Board {

    private String[][] board = new String[3][3];
    private Sign moveFor = null;

    public Board(String[][] board) {
        this.board = board;
    }

    public Board() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                //board[i][j] = i + " " + j;
                board[i][j] = Sign.EMPTY_CELL.getValue();
            }
        }
    }

    public void cross(int x, int y, Sign sign) {
        board[x][y] = sign.getValue();

        if(sign == Sign.X) {
            this.moveFor = Sign.Y;
        } else {
            this.moveFor = Sign.X;
        }
    }

    public void printBoard() {
        System.out.format("%3d%4d%4d %n", 1, 2, 3);

        System.out.println("A" + board[0][0] + "|" + board[0][1] + "|" + board[0][2]);
        System.out.println("-----------");
        System.out.println("B" + board[1][0] + "|" + board[1][1] + "|"
                + board[1][2]);
        System.out.println("-----------");
        System.out.println("C" + board[2][0] + "|" + board[2][1] + "|"
                + board[2][2]);

        System.out.println("/////////////////////////////////////////////////////");
    }

    public String[][] getBoard() {
        return board;
    }

    public Sign getMoveFor() {
        return moveFor;
    }

    public void setMoveFor(Sign moveFor) {
        this.moveFor = moveFor;
    }

}
