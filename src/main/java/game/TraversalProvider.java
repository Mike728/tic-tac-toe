package game;

import com.scalified.tree.TraversalAction;
import com.scalified.tree.TreeNode;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class TraversalProvider {

    public TraversalAction<TreeNode<BoardTreeNode>> traversalAction() {
        return new TraversalAction<>() {
            @Override
            public void perform(TreeNode<BoardTreeNode> node) {
                if (node.height() != 0) {
                    final List<BoardTreeNode> subtrees = node.subtrees().stream().map(TreeNode::data).collect(Collectors.toList());
                    if (node.height() % 2 != 0) {
                        node.setData(new BoardTreeNode(node.data().getBoard(), min(subtrees).getScore()));

                        if (node.isRoot()) {
                            node.setData(new BoardTreeNode(min(subtrees).getBoard(), min(subtrees).getScore()));
                        }

                    } else {
                        node.setData(new BoardTreeNode(node.data().getBoard(), max(subtrees).getScore()));

                        if (node.isRoot()) {
                            node.setData(new BoardTreeNode(max(subtrees).getBoard(), max(subtrees).getScore()));
                        }
                    }
                }
            }

            @Override
            public boolean isCompleted() {
                return false;
            }
        };
    }

    public static BoardTreeNode min(Collection<BoardTreeNode> coll) {
        Iterator<BoardTreeNode> i = coll.iterator();
        BoardTreeNode candidate = i.next();

        while (i.hasNext()) {
            BoardTreeNode next = i.next();
            if (Integer.valueOf(next.getScore()).compareTo(candidate.getScore()) < 0)
                candidate = next;
        }
        return candidate;
    }

    public static BoardTreeNode max(Collection<BoardTreeNode> coll) {
        Iterator<BoardTreeNode> i = coll.iterator();
        BoardTreeNode candidate = i.next();

        while (i.hasNext()) {
            BoardTreeNode next = i.next();
            if (Integer.valueOf(next.getScore()).compareTo(candidate.getScore()) > 0)
                candidate = next;
        }
        return candidate;
    }
}
