package game;

public class BoardTreeNode {

    private Board board;
    private int score;

    public BoardTreeNode() {
    }

    public BoardTreeNode(Board board, int score) {
        this.board = board;
        this.score = score;
    }

    public Board getBoard() {
        return board;
    }

    public int getScore() {
        return score;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
