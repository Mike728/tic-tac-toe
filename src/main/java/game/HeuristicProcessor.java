package game;

import java.util.Arrays;
import java.util.function.Predicate;

public class HeuristicProcessor {

    public int calculate(Board board) {
        int finalScore = 0;
        finalScore = finalScore + calculateForRows(board);

        return finalScore;
    }

    private int calculateForRows(Board board) {
        int partialScore = 0;
        final String[][] boardArray = board.getBoard();

        for (int i = 1; i <= 3; i++) {
            if(isThreeInALine(getNthRow(boardArray, i), Sign.X)) {
                partialScore = partialScore + 500;
            }

            if(isTwoInALine(getNthRow(boardArray, i), Sign.X)) {
                partialScore = partialScore + 10;
            }

            if(isOneInALine(getNthRow(boardArray, i), Sign.X)) {
                partialScore = partialScore + 1;
            }

            if(isThreeInALine(getNthColumn(boardArray, i), Sign.X)) {
                partialScore = partialScore + 500;
            }

            if(isTwoInALine(getNthColumn(boardArray, i), Sign.X)) {
                partialScore = partialScore + 10;
            }

            if(isOneInALine(getNthColumn(boardArray, i), Sign.X)) {
                partialScore = partialScore + 1;
            }

            if(isThreeInALine(getNthRow(boardArray, i), Sign.Y)) {
                partialScore = partialScore - 500;
            }

            if(isTwoInALine(getNthRow(boardArray, i), Sign.Y)) {
                partialScore = partialScore - 10;
            }

            if(isOneInALine(getNthRow(boardArray, i), Sign.Y)) {
                partialScore = partialScore - 1;
            }

            if(isThreeInALine(getNthColumn(boardArray, i), Sign.Y)) {
                partialScore = partialScore - 500;
            }

            if(isTwoInALine(getNthColumn(boardArray, i), Sign.Y)) {
                partialScore = partialScore - 10;
            }

            if(isOneInALine(getNthColumn(boardArray, i), Sign.Y)) {
                partialScore = partialScore - 1;
            }

            if(isBlockedWin(getNthColumn(boardArray, i), Sign.Y)) {
                partialScore = partialScore + 300;
            }

            if(isBlockedWin(getNthRow(boardArray, i), Sign.Y)) {
                partialScore = partialScore + 300;
            }

            if(isBlockedWin(getNthColumn(boardArray, i), Sign.X)) {
                partialScore = partialScore - 300;
            }

            if(isBlockedWin(getNthRow(boardArray, i), Sign.X)) {
                partialScore = partialScore - 300;
            }

        }

        if(isThreeInALine(getFirstDiagonal(boardArray), Sign.X)) {
            partialScore = partialScore + 500;
        }

        if(isTwoInALine(getFirstDiagonal(boardArray), Sign.X)) {
            partialScore = partialScore + 10;
        }

        if(isOneInALine(getFirstDiagonal(boardArray), Sign.X)) {
            partialScore = partialScore + 1;
        }

        if(isThreeInALine(getSecondDiagonal(boardArray), Sign.X)) {
            partialScore = partialScore + 500;
        }

        if(isTwoInALine(getSecondDiagonal(boardArray), Sign.X)) {
            partialScore = partialScore + 10;
        }

        if(isOneInALine(getSecondDiagonal(boardArray), Sign.X)) {
            partialScore = partialScore + 1;
        }

        if(isThreeInALine(getFirstDiagonal(boardArray), Sign.Y)) {
            partialScore = partialScore - 500;
        }

        if(isTwoInALine(getFirstDiagonal(boardArray), Sign.Y)) {
            partialScore = partialScore - 10;
        }

        if(isOneInALine(getFirstDiagonal(boardArray), Sign.Y)) {
            partialScore = partialScore - 1;
        }

        if(isThreeInALine(getSecondDiagonal(boardArray), Sign.Y)) {
            partialScore = partialScore - 500;
        }

        if(isTwoInALine(getSecondDiagonal(boardArray), Sign.Y)) {
            partialScore = partialScore - 10;
        }

        if(isOneInALine(getSecondDiagonal(boardArray), Sign.Y)) {
            partialScore = partialScore - 1;
        }

        if(isBlockedWin(getFirstDiagonal(boardArray), Sign.Y)) {
            partialScore = partialScore + 300;
        }

        if(isBlockedWin(getSecondDiagonal(boardArray), Sign.Y)) {
            partialScore = partialScore + 300;
        }

        if(isBlockedWin(getFirstDiagonal(boardArray), Sign.X)) {
            partialScore = partialScore - 300;
        }

        if(isBlockedWin(getSecondDiagonal(boardArray), Sign.X)) {
            partialScore = partialScore - 300;
        }


        return partialScore;
    }

    private String[] getNthRow(String[][] boardArray, int rowNumber) {
        String[] nthRow = { boardArray[rowNumber - 1][0], boardArray[rowNumber - 1][1], boardArray[rowNumber - 1][2] };
        return nthRow;
    }

    private String[] getNthColumn(String[][] boardArray, int columnNumber) {
        String[] nthColumn = { boardArray[0][columnNumber - 1], boardArray[1][columnNumber - 1], boardArray[2][columnNumber - 1] };
        return nthColumn;
    }

    private String[] getFirstDiagonal(String[][] boardArray) {
        String[] nthColumn = { boardArray[0][0], boardArray[1][1], boardArray[2][2] };
        return nthColumn;
    }


    private String[] getSecondDiagonal(String[][] boardArray) {
        String[] nthColumn = { boardArray[0][2], boardArray[1][1], boardArray[2][0] };
        return nthColumn;
    }

    private boolean isThreeInALine(String[] line, Sign sign) {
        return Arrays.stream(line).filter(is(sign)).count() == 3;
    }

    private boolean isTwoInALine(String[] line, Sign sign) {
        return Arrays.stream(line).filter(is(sign)).count() == 2;
    }

    private boolean isBlockedWin(String[] line, Sign sign) {
        return Arrays.stream(line).filter(is(sign)).count() == 2 &&
                Arrays.stream(line).filter(is(getOpposite(sign))).count() == 1;
    }

    private boolean isOneInALine(String[] line, Sign sign) {
        return Arrays.stream(line).filter(is(sign)).count() == 1;
    }

    private Predicate<String> isEmpty() {
        return v -> v.equals(Sign.EMPTY_CELL.getValue());
    }

    private Predicate<String> is(Sign sign) {
        return v -> v.equals(sign.getValue());
    }

    private Sign getOpposite(Sign sign) {
        return sign == Sign.X ? Sign.Y : Sign.X;
    }
}
