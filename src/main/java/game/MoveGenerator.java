package game;

import com.scalified.tree.TreeNode;
import com.scalified.tree.multinode.ArrayMultiTreeNode;

import java.util.ArrayList;
import java.util.List;

public class MoveGenerator {

    public TreeNode<BoardTreeNode> generateMovesTree(Board board) {
        final HeuristicProcessor heuristicProcessor = new HeuristicProcessor();

        final String[][] boardArray = board.getBoard();
        final Sign sign = board.getMoveFor();

        final int calculate = heuristicProcessor.calculate(board);

        TreeNode<BoardTreeNode> node = new ArrayMultiTreeNode<>(new BoardTreeNode(board, calculate));

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (isEmpty(boardArray[i][j])) {
                    final String[][] possibleMove = copyArray(boardArray);

                    possibleMove[i][j] = sign.getValue();
                    final Board newBoard = new Board(possibleMove);
                    newBoard.setMoveFor(getOpposite(sign));

                    node.add(generateMovesTree(newBoard));
                }
            }
        }

        return node;
    }

    private boolean isEmpty(String s) {
        return s.equals(Sign.EMPTY_CELL.getValue());
    }

    private Sign getOpposite(Sign sign) {
        return sign == Sign.X ? Sign.Y : Sign.X;
    }

    public String[][] copyArray(String[][] old) {
        String[][] newArray = new String[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                newArray[i][j] = old[i][j];
            }
        }
        return newArray;
    }
}
